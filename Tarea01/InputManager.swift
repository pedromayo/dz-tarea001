//
//  InputManager.swift
//  Tarea01
//
//  Created by pedro mayo on 4/04/22.
//
import Foundation



struct InputManager {
    
    private func validate(usingInputTextField inputTextField:InputValidatable)->Bool{
        let rules = inputTextField.validationRules
        var isValid = true
        rules.forEach { (rule) in
            if !rule.validate(value: inputTextField.validationText){
                isValid = false
            }
        }
        return  isValid
    }
    
    func getStudentInfo(message:String)->String{
        var tf : InputTextField
        var isValid = false
        repeat {
            print(message)
            tf = InputTextField(rules: [RequiredRule(),NoNumbersRule(),MinimunLengthRule()])
            tf.inputText = readLine() ?? ""
            isValid = validate(usingInputTextField: tf)
            if !isValid{
                tf.setErrorMessage()
            }
           
        } while isValid == false
        return  tf.validationText
    }  
    
    
    func getDNI()->String{
        var dnitf : InputTextField
        var isValid = false
        repeat {
            print("Enter DNI")
            dnitf = InputTextField(rules: [RequiredRule(), IntRule(),DNILengthRule()])
            dnitf.inputText = readLine() ?? ""
            isValid = validate(usingInputTextField: dnitf)
            if !isValid{
                dnitf.setErrorMessage()
            }
           
        } while isValid == false
        return  dnitf.validationText
    }
    
    func getBirdthDate()->String{
        var  tf : InputTextField
        var isValid = false
        repeat {
            print("Enter Birthday Date in format dd/MM/yy")
            tf = InputTextField(rules: [RequiredRule(), BirthdayRule()])
            tf.inputText = readLine() ?? ""
            isValid = validate(usingInputTextField: tf)
            if !isValid{
                tf.setErrorMessage()
            }
           
        } while isValid == false
        return  tf.validationText
    }
    
    
    
    func getIntWith(_ message: String, errorMessage: String, withRange range: ClosedRange<Int>) -> Int {
        
        var isCorrect = false
        var number = 0
        repeat {
            print(message)
            if let inputValue = readLine(), inputValue.count > 0, let value = Int(inputValue), range.contains(value) {
                number = value
                isCorrect = true
            } else {
                self.showErrorMessage(errorMessage)
            }
            
        } while !isCorrect
        
        return number
    }
    
    
    func getStringWith(_ message: String, errorMessage: String) -> String {
        
        var input = ""
        repeat {
            print(message)
            if let inputValue = readLine(), inputValue.count > 0 {
                input = inputValue
            } else {
                self.showErrorMessage(errorMessage)
            }
            
        } while input.count == 0
        
        return input
    }
    private func showErrorMessage(_ errorMessage: String) {
        
        let message = """
        ==============================================
        ERROR EN EL VALOR: \(errorMessage)
        ==============================================
        
        
        """
        print(message)
    }
    
    
}
