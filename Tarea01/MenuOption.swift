//
//  MenuOption.swift
//  Tarea01
//
//  Created by pedro mayo on 4/04/22.
//

import Foundation

enum MenuOption:Int,CaseIterable{
    case none = 0
    case add
    case list
    case search
    case delete
    case exit
}

 
struct Menu {
    
   
    func show() {
        let message = """
        ====================================
                   MI APLICACIÓN
        ====================================
        1. Add student
        2. List students
        3. Search student
        4. Delete student
        5. Exit
        
        """
        self.clearConsole()
        print(message)
    }
    
    private func clearConsole() {
        let separator = Array(repeating: "\n", count: 5).joined()
        print("", terminator: separator)
    }
}

