//
//  Student.swift
//  Tarea01
//
//  Created by pedro mayo on 4/04/22.
//

import Foundation
struct Student {
    var name: String
    var lastName: String
    var address: String
    var dni : String
    var birthDate:Date
    
    var formatedBirthDate:String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: birthDate)
    }
    
    var nameInitials: String {
           guard let nameInital = self.name.first, let lastNameInitial = self.lastName.first else { return "" }
           return "\(nameInital)\(lastNameInitial)".uppercased()
       }

       var fullName: String {
           return "\(self.lastName) \(self.name)".capitalized
    }

    
    var displayBasicInfo: String {
        "\(nameInitials),\(fullName)"
    }
    
    var displaySearchInfo: String {
        "\(formatedBirthDate),\(dni),\(age),\(fullName),\(address),\(nameInitials)"
    }
    
    var age :Int{
        birthDate.age
    }
    
}

