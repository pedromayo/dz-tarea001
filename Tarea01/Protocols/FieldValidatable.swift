//
//  Validatable.swift
//  Tarea01
//
//  Created by pedro mayo on 10/04/22.
//

import Foundation

protocol Rule {
    func validate(value:String)->Bool
    func getErrorMessage()->String
}

protocol FieldValidatable {
    var validationRules:[Rule]{get}
    var validationText:String{get}
}

protocol InputValidatable{
    var validationRules:[Rule]{get}
    var validationText:String{get}
}

