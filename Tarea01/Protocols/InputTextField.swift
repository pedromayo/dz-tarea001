//
//  InputTextField.swift
//  Tarea01
//
//  Created by pedro mayo on 10/04/22.
//

import Foundation

struct InputTextField{
    var inputText : String = ""
    var rules : [Rule]
    var errorMessage : String = ""
}
extension InputTextField:InputValidatable{
    
    var validationRules: [Rule] {
        get{return rules}
        set{rules = newValue}
    }
    
    var validationText: String {
         return inputText
    }
    
    mutating func setRule(rules:[Rule]){
        validationRules.removeAll()
        validationRules.append(contentsOf: rules)
    }
    
   mutating func setErrorMessage()  {
    errorMessage =
        self.rules
        .filter({!$0.validate(value:self.validationText)})
        .first
        .map({$0.getErrorMessage()}) ?? ""
        print(errorMessage)
   }
     
}
