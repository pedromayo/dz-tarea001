//
//  Rules.swift
//  Tarea01
//
//  Created by pedro mayo on 10/04/22.
//

import Foundation

open class RequiredRule:Rule{
   
   private var errorMessage : String
   
   init(errorMessage:String = "Este valor es requerido") {
       self.errorMessage = errorMessage
   }
   func validate(value: String) -> Bool {
       return !value.isEmpty
   }
   
   func getErrorMessage() -> String {
       return errorMessage
   }
}

open class IntRule:Rule{
   
   private var errorMessage : String
   
   init(errorMessage:String = "Debe Contener Solo Numeros") {
       self.errorMessage = errorMessage
   }
   func validate(value: String) -> Bool {
       if Int(value) != nil{
           return true
       }
       return false
       
   }
   
   
   func getErrorMessage() -> String {
       return errorMessage
   }
}

open class NoNumbersRule:Rule{
   
   private var errorMessage : String
   
   init(errorMessage:String = "El dato no debe contener numeros") {
       self.errorMessage = errorMessage
   }
   func validate(value: String) -> Bool {
       for character in value{
               if character.isNumber{
                   return false
               }
           }
           return true
   }
   
   
   func getErrorMessage() -> String {
       return errorMessage
   }
}


class DNILengthRule:Rule{
   let MIN_LEGTH = 8
   private var errorMessage :String
   init(errorMessage:String = "DNI debe contener 8 digitos") {
       self.errorMessage = errorMessage
   }
   func validate(value: String) -> Bool {
       return value.count == MIN_LEGTH
   }
   func getErrorMessage() -> String {
    errorMessage
   }
}
class BirthdayRule:Rule{
   private var errorMessage :String
   init(errorMessage:String = "No tiene el formato correcto dd/MM/yy") {
       self.errorMessage = errorMessage
   }
    func validate(value: String) -> Bool {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd/MM/yy"
        guard dateFormater.date(from: value) != nil else { return false  }
        return true
    }
    
   func getErrorMessage() -> String {
    errorMessage
   }
}

class MinimunLengthRule:Rule{
   let MIN_LEGTH = 2
   private var errorMessage :String
   init(errorMessage:String = "Debe contener minimo 2 caracteres") {
       self.errorMessage = errorMessage
   }
   func validate(value: String) -> Bool {
       return value.count >= MIN_LEGTH
   }
   func getErrorMessage() -> String {
    errorMessage
   }
}
