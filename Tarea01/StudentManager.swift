//
//  StudentManager.swift
//  Tarea01
//
//  Created by pedro mayo on 4/04/22.
//

import Foundation

struct StudentManager{
    
    private var students : [Student] = [Student]()
    
    
    func listStudents(){
        switch students.count {
        case 0: print("No hay estudiantes")
        default:
            print("""
                *******  List of Students ******
                  
                """)
            let sortedStudents = students.sorted {$0.lastName.lowercased() < $1.lastName.lowercased()}
             
            sortedStudents.enumerated().forEach {
                print("\($0+1))\($1.displayBasicInfo)\n")
            }
        }
    }
    
    mutating func deleteStudentByDNI(_ dni :String){
        let student = students.first { $0.dni == dni }
        guard let foundStudent = student else {
            print("Cant delete . No student found with dni : \(dni)")
            return
        }
        
        let value = inputManager.getIntWith("Esta seguro de eliminar al estudiante \(foundStudent.fullName)? Presione 1 para eliminar . 2 para no eliminar",
                                            errorMessage: "La opción ingresa es incorrecta",
                                            withRange: 1...2)
        switch value {
        case 1:
            students.removeAll { $0.dni == foundStudent.dni}
            print("Student deleted with success")
        default:
            print("Student was not deleted")
            menu.show()
        }
        
       
    }
    
    func searchStudentByDNI(_ dni:String){
        let student = students.first { $0.dni == dni }
        guard let foundStudent = student else {
            print("No student found with dni : \(dni)")
            return
        }
        print("Student Found:\n" +
                "\(foundStudent.displaySearchInfo)")
    }
    
    
    mutating func registerWith(name:String,lastName:String,address:String,dni:String,birthDate:String){
        
        let student =  Student(name: name, lastName: lastName, address: address, dni: dni, birthDate: birthDate.convertToDate() ?? Date())
        self.students.append(student)
        print("Student registered with success")
        
    }
}




