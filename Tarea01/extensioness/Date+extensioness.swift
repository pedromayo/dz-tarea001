//
//  Date+extensioness.swift
//  Tarea01
//
//  Created by pedro mayo on 11/04/22.
//

import Foundation
extension Date {
    var age: Int { Calendar.current.dateComponents([.year], from: self, to: Date()).year! }
}
