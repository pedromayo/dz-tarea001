//
//  String+extensionss.swift
//  Tarea01
//
//  Created by pedro mayo on 11/04/22.
//

import Foundation
extension String{
    func convertToDate()->Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy"
       let date =  dateFormatter.date(from:self)
        return date ?? Date()
    }
}
