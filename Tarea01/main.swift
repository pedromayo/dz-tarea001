//
//  main.swift
//  Tarea01
//
//  Created by pedro mayo on 4/04/22.
//

import Foundation

 

var option: MenuOption
let menu = Menu()
let inputManager = InputManager()
var studentManager = StudentManager()
let numberOfOptions = (MenuOption.allCases.count - 1)

repeat {
    
    menu.show()
    let value = inputManager.getIntWith("Ingrese el número de opción que desea ver",
                                        errorMessage: "La opción ingresa es incorrecta",
                                        withRange: 1...numberOfOptions)
    option = MenuOption(rawValue: value) ?? .none
    
    switch option {
        case .none: print("OPCIÓN NO RECONOCIDA")
                 break
    case .add:
        let name =  inputManager.getStudentInfo(message: "Enter Name : ")
        let lastName =   inputManager.getStudentInfo(message: "Enter Last Name :")
        let address =       inputManager.getStringWith("Enter AddrEss", errorMessage: "Empty Value - must enter Address")
        let dni = inputManager.getDNI()
         let birthDate =  inputManager.getBirdthDate()

        studentManager.registerWith(name: name, lastName: lastName, address: address, dni: dni, birthDate: birthDate)
        break
        
    case .list: studentManager.listStudents()
        break
        
    case .exit:
        break
        
    case .search:
        let dni = inputManager.getDNI()
        studentManager.searchStudentByDNI(dni)
        break
        
    case .delete:
        let dni = inputManager.getDNI()
        studentManager.deleteStudentByDNI(dni)
        break
    }
    
    let _ = readLine()
        
} while option != .exit && option != .none

